/*
    This class connects to the API https://anapioficeandfire.com/ and fetches data from various endpoints.

    Data fetched by this class is returned as Strings.
 */


package se.experis.connector;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConnector {
    private final String CHARACTER_API = "https://anapioficeandfire.com/api/characters/";
    private final String HOUSES_API = "https://anapioficeandfire.com/api/houses/";
    private final String BOOK_API = "https://anapioficeandfire.com/api/books/";
    private final String SWORN_MEMBERS = "swornMembers";

    public String requestCharacter(String id){
        try {
            HttpURLConnection con = getConnection(CHARACTER_API + id);

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return getResponseContent(con);
            }
            else {
                System.out.println("Error!");
                System.out.println("Response was: " + con.getResponseCode());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String requestSwornMembersOfHouse(String houseUrl) {
        try {
            HttpURLConnection con = getConnection(houseUrl);

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String content = getResponseContent(con);

                Gson gson = new Gson();
                JsonObject jsonObject = gson.fromJson(content, JsonObject.class);

                // Return only the "swornMembers" field of the fetched data.
                return jsonObject.getAsJsonArray(SWORN_MEMBERS).toString();
            }
            else {
                System.out.println("Error!");
                System.out.println("Response was: " + con.getResponseCode());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String requestNameOfHouse(String houseUrl) {
        try {
            HttpURLConnection con = getConnection(houseUrl);

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String content = getResponseContent(con);

                Gson gson = new Gson();
                JsonObject jsonObject = gson.fromJson(content, JsonObject.class);

                return jsonObject.get("name").getAsString();
            }
            else {
                System.out.println("Error!");
                System.out.println("Response was: " + con.getResponseCode());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String requestBooks() {
        try {
            HttpURLConnection con = getConnection(BOOK_API);

            String content = getResponseContent(con);

            return content;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Sets up connection and sets the request to type "GET", to the input url */
    private HttpURLConnection getConnection(String inputUrl) {
        try {
            URL url = new URL(inputUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            return con;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Reads response content data and returns it as a string */
    private String getResponseContent(HttpURLConnection con) {
        try {
            InputStreamReader isr = new InputStreamReader(con.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = br.readLine()) != null) {
                content.append(inputLine);
            }
            br.close();
            return content.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
