/*
    This class handles input and output to the console.
 */

package se.experis.view;

import se.experis.ice_and_fire.Character;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {

    private final String ANSI_RESET = "\u001B[0m";
    private final String ANSI_GREEN = "\u001B[32m";

    private String userInput;

    public void askForCharacterNumber() {
        System.out.println("Enter a number to look up character: ");
    }

    public String getUserInput() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void requireUserInput() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            userInput = reader.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void displayCharacterInfo(Character character) {
        System.out.println(ANSI_GREEN + "\n--- CHARACTER INFO ---" + ANSI_RESET);
        System.out.println("Name: " + character.getName() +
                "\nGender: " + character.getGender() +
                "\nCulture: " + character.getCulture() +
                "\nBorn: " + character.getBorn()+
                "\nDied: " + character.getDied() +
                "\nTitles: " + character.getTitles().toString() +
                "\nAliases: " + character.getAliases().toString() +
                "\nFather: " + character.getFather() +
                "\nMother: " + character.getMother() +
                "\nSpouse: " + character.getSpouse() +
                "\nAllegiances: " + character.getAllegiances().toString() +
                "\nBooks: " + character.getBooks().toString() +
                "\npovBooks: " + character.getPovBooks().toString() +
                "\nTv-Series: " + character.getTvSeries() +
                "\nPlayed by: " + character.getPlayedBy());
    }

    public void displayCharacterName(String name) {
        System.out.println("Name: " + name);
    }

    public void displayHouseName(String house) {
        System.out.println(ANSI_GREEN + "\n\n----- " + house.toUpperCase() + " -----" + ANSI_RESET);
    }

    public void displayOptionMenu() {
        System.out.println("\nDo you want to display all sworn members of the previous character? (y/n): ");
    }

    public boolean wantsToDisplaySwornMembers() {
        return userInput.equalsIgnoreCase("y");
    }

    public void displayPovCharacterHeader() {
        System.out.println(ANSI_GREEN + "\n----- POV CHARACTERS OF THE BANTAM BOOKS -----" + ANSI_RESET);
    }

    public void displayBookTitle(String bookTitle) {
        System.out.println(ANSI_GREEN + "\n-- " + bookTitle.toUpperCase() + " --" + ANSI_RESET);
    }

    public void pressEnterKeyToContinue() {
        System.out.println("\nPress Enter key to continue...");
        try {
            System.in.read();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
