package se.experis;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import se.experis.connector.HttpConnector;
import se.experis.ice_and_fire.Book;
import se.experis.ice_and_fire.Character;
import se.experis.view.Console;

import java.util.ArrayList;

public class Main {
    private static Console console = new Console();
    private static HttpConnector connector = new HttpConnector();
    private static Gson gson = new Gson();

    public static void main(String[] args) {

        /* ASK FOR AN ID OF A CHARACTER TO FETCH FROM API */
        console.askForCharacterNumber();
        String id = console.getUserInput();

        /* CREATE AND SHOW CHARACTER INFO */
        String characterJson = connector.requestCharacter(id);
        Character character = gson.fromJson(characterJson, Character.class);
        console.displayCharacterInfo(character);

        /* CHECK IF USER WANTS TO DISPLAY SWORN MEMBERS */
        console.displayOptionMenu();
        console.requireUserInput();
        if (console.wantsToDisplaySwornMembers()) {
            displayAllSwornMembers(character.getAllegiances());

            console.pressEnterKeyToContinue();
        }

        /* FINALLY DISPLAY ALL POV CHARACTERS */
        displayPoVCharacters();

    }

    /* Find all sworn members to the inputted allegiance and print their names */
    private static void displayAllSwornMembers(ArrayList<String> allegiances) {
        for (String allegiance : allegiances) {
            String membersJson = connector.requestSwornMembersOfHouse(allegiance);

            ArrayList<String> members = gson.fromJson(membersJson, ArrayList.class);

            console.displayHouseName(connector.requestNameOfHouse(allegiance));

            Character member;
            for (String m : members) {
                String memberJson = connector.requestCharacter(m.substring(m.lastIndexOf("/") + 1));

                member = gson.fromJson(memberJson, Character.class);
                console.displayCharacterName(member.getName());
            }
        }

    }

    /* Find and print all pov characters of the bantam books. */
    private static void displayPoVCharacters() {
        String booksJson = connector.requestBooks();

        JsonArray jsonArray = gson.fromJson(booksJson, JsonArray.class);

        console.displayPovCharacterHeader();

        /* ITERATE ALL BOOKS FETCHED FROM API */
        for (JsonElement obj : jsonArray) {
            Book book = gson.fromJson(obj, Book.class);

            console.displayBookTitle(book.getName());

            if (book.getPublisher().equalsIgnoreCase("Bantam Books")) {
                Character character;
                for (String povCharacter : book.getPovCharacters()) {
                    character = gson.fromJson(connector.requestCharacter(povCharacter.substring(povCharacter.lastIndexOf("/") + 1)), Character.class);

                    console.displayCharacterName(character.getName());
                }
            }
        }
    }
}
