package se.experis.ice_and_fire;

import java.util.ArrayList;

public class Book {
    private String name;
    private String publisher;
    private ArrayList<String> povCharacters;

    public Book(String name, String publisher, ArrayList<String> povCharacters) {
        this.name = name;
        this.publisher = publisher;
        this.povCharacters = povCharacters;
    }

    public String getName() {
        return name;
    }

    public String getPublisher() {
        return publisher;
    }

    public ArrayList<String> getPovCharacters() {
        return povCharacters;
    }
}
