package se.experis.ice_and_fire;

import java.util.ArrayList;

public class Character {
    private String name;
    private String gender;
    private String culture;
    private String born;
    private String died;
    private ArrayList<String> titles;
    private ArrayList<String> aliases;
    private String father;
    private String mother;
    private String spouse;
    private ArrayList<String> allegiances;
    private ArrayList<String> books;
    private ArrayList<String> povBooks;
    private ArrayList<String> tvSeries;
    private ArrayList<String> playedBy;

    public Character(String name, String gender, String culture, String born, String died, ArrayList<String> titles, ArrayList<String> aliases, String father, String mother, String spouse, ArrayList<String> allegiances, ArrayList<String> books, ArrayList<String> povBooks, ArrayList<String> tvSeries, ArrayList<String> playedBy) {
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.spouse = spouse;
        this.allegiances = allegiances;
        this.books = books;
        this.povBooks = povBooks;
        this.tvSeries = tvSeries;
        this.playedBy = playedBy;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getCulture() {
        return culture;
    }

    public String getBorn() {
        return born;
    }

    public String getDied() {
        return died;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

    public ArrayList<String> getAliases() {
        return aliases;
    }

    public String getFather() {
        return father;
    }

    public String getMother() {
        return mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public ArrayList<String> getAllegiances() {
        return allegiances;
    }

    public ArrayList<String> getBooks() {
        return books;
    }

    public ArrayList<String> getPovBooks() {
        return povBooks;
    }

    public ArrayList<String> getTvSeries() {
        return tvSeries;
    }

    public ArrayList<String> getPlayedBy() {
        return playedBy;
    }

}
